//
//  SliderColorView.swift
//  ColoringViewSUI_HW
//
//  Created by Maksim on 17.12.2023.
//

import SwiftUI

struct SliderColorView: View {
    @Binding var value: Double
    @State private var text = ""
    
    let color: Color
    
    var body: some View {
        HStack {
            TextView(value: value)
            
            Slider(value: $value, in: 0...255, step: 1)
                .tint(color)
                .onChange(of: value) { newValue in
                    text = "\(lround(newValue))"
                }
            
            TextFieldView(text: $text, value: $value)
        }
        .onAppear {
            text = "\(lround(value))"
        }
    }
}

struct SliderColor_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.gray
            SliderColorView(value: .constant(55), color: .red)
        }
    }
}
