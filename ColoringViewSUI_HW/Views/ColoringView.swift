//
//  ColoringView.swift
//  ColoringViewSUI_HW
//
//  Created by Maksim on 17.12.2023.
//

import SwiftUI

struct ColoringView: View {
    let red: Double
    let green: Double
    let blue: Double
    
    var body: some View {
        Color(red: red / 255, green: green / 255, blue: blue / 255)
            .frame(height: 200)
            .cornerRadius(20)
            .overlay(
                RoundedRectangle(cornerRadius: 20)
                    .stroke(Color .white, lineWidth: 5)
            )
    }
}

struct ColoringView_Previews: PreviewProvider {
    static var previews: some View {
        ColoringView(red: 80, green: 90, blue: 110)
    }
}
