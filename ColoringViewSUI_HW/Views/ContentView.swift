//
//  ContentView.swift
//  ColoringViewSUI_HW
//
//  Created by Maksim on 17.12.2023.
//

import SwiftUI

struct ContentView: View {
    @State private var red = Double.random(in: 0...255)
    @State private var green = Double.random(in: 0...255)
    @State private var blue = Double.random(in: 0...255)
    
    @FocusState private var isInputActive: Bool
    
    var body: some View {
        ZStack {
            Color.gray.ignoresSafeArea()
                .onTapGesture {
                    isInputActive = false
                }
            
            VStack(spacing: 30) {
                ColoringView(red: red, green: green, blue: blue)
                
                VStack(spacing: 20) {
                    SliderColorView(value: $red, color: .red)
                    SliderColorView(value: $green, color: .green)
                    SliderColorView(value: $blue, color: .blue)
                }
                .frame(height: 240)
                .focused($isInputActive)
                .toolbar {
                    ToolbarItemGroup(placement: .keyboard) {
                        Spacer()
                        Button("Done") {
                            isInputActive = false
                        }
                    }
                }
                Spacer()
            }
            .padding(20)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
