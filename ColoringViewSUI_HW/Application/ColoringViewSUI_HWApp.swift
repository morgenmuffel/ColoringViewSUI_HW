//
//  ColoringViewSUI_HWApp.swift
//  ColoringViewSUI_HW
//
//  Created by Maksim on 17.12.2023.
//

import SwiftUI

@main
struct ColoringViewSUI_HWApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
